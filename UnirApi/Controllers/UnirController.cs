﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UnirApi.Model;

namespace UnirApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnirController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetData()
        {
            List<Custumer> data = new List<Custumer>() {
            new Custumer()
            {
                Id = 1,
                Universidad = "UNIR",
                Nombre = "Amorhin Rojas H.",
                Asiganatura = "Contenedores"
            },
            new Custumer()
            {
                Id = 1,
                Universidad = "UNIR",
                 Nombre = "Amorhin Rojas H.",
                Asiganatura = "Gestion de proyectos"
            }
            };

            return Ok(data);
        }
    }
}

